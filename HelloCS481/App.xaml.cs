﻿using System.Diagnostics;
using Xamarin.Forms;

namespace HelloCS481
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new HelloCS481Page();
        }

        protected override void OnStart()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnStart)}");
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnSleep)}");
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            Debug.WriteLine($"**** {this.GetType().Name}.{nameof(OnResume)}");
            // Handle when your app resumes
        }
    }
}
